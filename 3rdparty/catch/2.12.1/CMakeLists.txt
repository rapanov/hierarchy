# Copyright 2020, Roman Panov (roman.a.panov@gmail.com).

add_library(catch INTERFACE)

target_include_directories(
  catch
  INTERFACE
  "${CMAKE_CURRENT_SOURCE_DIR}")
