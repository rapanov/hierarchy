/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#include <cstdio>
#include <hierarchy_server.hh>

class Input
{
  class End
  {};

  struct Iterator
  {
    char operator *() const
    {
      if (input_.char_valid_)
      {
        input_.char_valid_ = false;
      }
      else
      {
        input_.read();
      }

      return static_cast<char>(input_.char_);
    }

    Iterator& operator ++() noexcept
    {
      return *this;
    }

    Iterator& operator ++(const int) noexcept
    {
      return *this;
    }

    friend bool operator ==(const Iterator& it, const End&)
    {
      return it.done();
    }

    friend bool operator !=(const Iterator& it, const End&)
    {
      return !it.done();
    }

  private:
    friend class Input;

    explicit Iterator(Input& input) noexcept
      : input_(input)
    {}

    bool done() const noexcept
    {
      return input_.done();
    }

    Input& input_;
  };

public:
  Input() noexcept
    : char_()
    , char_valid_(false)
    , f_(stdin)
  {}

  Iterator begin() noexcept
  {
    return Iterator(*this);
  }

  static End end() noexcept
  {
    return {};
  }

private:
  bool done()
  {
    if (!char_valid_)
    {
      read();
      char_valid_ = true;
    }

    return char_ == EOF;
  }

  void read()
  {
    char_ = ::std::fgetc(f_);
  }

  int char_;
  bool char_valid_;
  ::std::FILE* const f_;
};

struct Output
{
  Output() noexcept
    : f_(stdout)
  {}

  void put(const char c)
  {
    ::std::fputc(c, f_);
  }

  void flush()
  {
    ::std::fflush(f_);
  }

private:
  ::std::FILE* const f_;
};

int main()
{
  ::Hierarchy_server server;
  server.run(Input(), Output());
  return 0;
}
