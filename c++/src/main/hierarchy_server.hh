/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#ifndef HIERARCHYSERVER_HH
#define HIERARCHYSERVER_HH

#include <cctype>
#include <memory>
#include <optional>
#include <set>
#include <string>
#include <type_traits>
#include <cxx/util/hierarchy.hh>
#include <cxx/util/json/object.hh>
#include <cxx/util/json/parser.hh>

struct Hierarchy_server
{
  template <class Input, class Output>
  void run(Input&& input, Output&& output)
  {
    decltype(auto) in_it = static_cast<Input&&>(input).begin();
    decltype(auto) in_it_end = static_cast<Input&&>(input).end();

    using Iterator = Decay<decltype(in_it)>;
    using Iterator_end = Decay<decltype(in_it_end)>;
    using One_liner = One_liner<Iterator, Iterator_end>;

    Parser parser;

    while (in_it != in_it_end)
    {
      const Object request = parser.parse(One_liner(in_it, in_it_end));
      process_request(request, static_cast<Output&&>(output));
    }
  }

private:
  template <class T>
  using Decay = typename ::std::decay<T>::type;

  template <class T>
  using Optional = ::std::optional<T>;

  template <class T>
  using Set = ::std::set<T>;

  using String = ::std::string;
  using Hierarchy = ::cxx::util::Hierarchy;
  using Object = ::cxx::util::json::Object;
  using Parser = ::cxx::util::json::Parser;

  template <class Output>
  void process_request(const Object& request, Output&& output)
  {
    if (request.empty())
    {
      return;
    }

    for (const auto& entry : request)
    {
      const auto& command = entry.first;

      if (command == "add_node")
      {
        add_node(entry.second, static_cast<Output&&>(output));
        return;
      }

      if (command == "delete_node")
      {
        delete_node(entry.second, static_cast<Output&&>(output));
        return;
      }

      if (command == "move_node")
      {
        move_node(entry.second, static_cast<Output&&>(output));
        return;
      }

      if (command == "query")
      {
        query(entry.second, static_cast<Output&&>(output));
        return;
      }
    }
  }

  template <class Output>
  void add_node(const Object::Value& request, Output&& output)
  {
    print_result(add_node(request), static_cast<Output&&>(output));
  }

  template <class Output>
  void delete_node(const Object::Value& request, Output&& output)
  {
    print_result(delete_node(request), static_cast<Output&&>(output));
  }

  template <class Output>
  void move_node(const Object::Value& request, Output&& output)
  {
    print_result(move_node(request), static_cast<Output&&>(output));
  }

  template <class Output>
  void query(const Object::Value& request, Output&& output)
  {
    using Depth = Hierarchy::Depth;

    struct Consumer
    {
      Consumer(
        Output&& output,
        const Optional<Set<String>>& node_ids,
        const Optional<Set<String>>& node_names) noexcept
        : saw_node(false)
        , output_(static_cast<Output&&>(output))
        , node_ids_(node_ids)
        , node_names_(node_names)
      {}

      void operator ()(
        const Hierarchy::String& id,
        const Hierarchy::String& name,
        const Hierarchy::String& parent_id)
      {
        if (node_ids_ && node_ids_->count(id) <= 0U)
        {
          return;
        }

        if (node_names_ && node_names_->count(name) <= 0U)
        {
          return;
        }

        if (saw_node)
        {
          print(",\n", static_cast<Output&&>(output_));
        }
        else
        {
          saw_node = true;
        }

        print("        {\n", static_cast<Output&&>(output_));

        print("            ", static_cast<Output&&>(output_));
        print_entry("id", id.data(), static_cast<Output&&>(output_));
        print(",\n", static_cast<Output&&>(output_));

        print("            ", static_cast<Output&&>(output_));
        print_entry("name", name.data(), static_cast<Output&&>(output_));
        print(",\n", static_cast<Output&&>(output_));

        print("            ", static_cast<Output&&>(output_));
        print_entry(
          "parent_id", parent_id.data(), static_cast<Output&&>(output_));
        print("\n", static_cast<Output&&>(output_));

        print("        }\n", static_cast<Output&&>(output_));
      }

      bool saw_node;

    private:
      Output&& output_;
      const Optional<Set<String>>& node_ids_;
      const Optional<Set<String>>& node_names_;
    };

    if (!request.is_object())
    {
      print_result(false, static_cast<Output&&>(output));
      return;
    }

    const Object& obj = request.object();
    Optional<Depth> min_depth;
    Optional<Depth> max_depth;
    Optional<Set<String>> node_ids;
    Optional<Set<String>> node_names;
    const Object::Value::Array* root_ids = nullptr;

    for (const auto& entry : obj)
    {
      const auto& key = entry.first;
      const auto& value = entry.second;

      if (value.is_string())
      {
        if (key == "min_depth")
        {
          min_depth = parse_int<Depth>(value.string().data());
        }
        else if (key == "max_depth")
        {
          max_depth = parse_int<Depth>(value.string().data());
        }
      }
      else if (value.is_array())
      {
        Set<String>* set_ptr = nullptr;

        if (key == "ids")
        {
          node_ids.emplace();
          set_ptr = ::std::addressof(*node_ids);
        }
        else if (key == "names")
        {
          node_names.emplace();
          set_ptr = ::std::addressof(*node_names);
        }
        else if (key == "root_ids")
        {
          root_ids = ::std::addressof(value.array());
        }

        if (!set_ptr)
        {
          continue;
        }

        for (const auto& elem : value.array())
        {
          if (elem.is_string())
          {
            set_ptr->insert(elem.string());
          }
        }

        continue;
      }
    }

    print("{\n", static_cast<Output&&>(output));
    print("    \"nodes\": [\n", static_cast<Output&&>(output));

    Consumer consumer(static_cast<Output&&>(output), node_ids, node_names);

    if (root_ids)
    {
      for (const auto root_id : *root_ids)
      {
        if (root_id.is_string())
        {
          hierarchy_.query(root_id.string(), min_depth, max_depth, consumer);
        }
      }
    }
    else
    {
      hierarchy_.query({}, min_depth, max_depth, consumer);
    }

    if (consumer.saw_node)
    {
      print("\n", static_cast<Output&&>(output));
    }

    print("    ]\n", static_cast<Output&&>(output));
    print("}\n", static_cast<Output&&>(output));
    static_cast<Output&&>(output).flush();
  }

  bool add_node(const Object::Value& request);
  bool delete_node(const Object::Value& request);
  bool move_node(const Object::Value& request);

  template <class Output>
  static void print_result(const bool res, Output&& output)
  {
    print("{\"ok\":", static_cast<Output&&>(output));
    print(res ? "true" : "false", static_cast<Output&&>(output));
    print("}\n", static_cast<Output&&>(output));
    static_cast<Output&&>(output).flush();
  }

  template <class Output>
  static void print(const char* str, Output&& output)
  {
    while (*str)
    {
      static_cast<Output&&>(output).put(*str);
      ++str;
    }
  }

  template <class Output>
  static void print_entry(
    const char* const key, const char* const value, Output&& output)
  {
    print("\"", static_cast<Output&&>(output));
    print(key, static_cast<Output&&>(output));
    print("\": \"", static_cast<Output&&>(output));
    print(value, static_cast<Output&&>(output));
    print("\"", static_cast<Output&&>(output));
  }

  template <class T>
  static Optional<T> parse_int(const char* str)
  {
    bool start = true, end = false, neg = false;
    T val = T(0);

    for (;; ++str)
    {
      const char ch = *str;

      if (!ch)
      {
        break;
      }

      if (ch >= '0' && ch <= '9')
      {
        if (end)
        {
          return {};
        }

        start = false;
        val *= T(10);
        val += T(ch - '0');
      }
      else if (ch == '-')
      {
        if (start)
        {
          start = false;
          neg = true;
        }
        else
        {
          return {};
        }
      }
      else if (is_space(ch))
      {
        if (!start)
        {
          end = true;
        }
      }
      else
      {
        return {};
      }
    }

    if (neg)
    {
      val = -val;
    }

    return val;
  }

  static bool is_ascii(const char ch) noexcept
  {
    return static_cast<unsigned char>(ch) < 0x80U;
  }

  static bool is_space(const char ch) noexcept
  {
    return is_ascii(ch) && ::std::isspace(ch);
  }

  static Optional<String> get_string(const Object& obj, const String& key);

  template <class Base_iterator, class Base_end>
  struct One_liner
  {
    One_liner(Base_iterator& base_it, const Base_end& base_end) noexcept
      : base_it_(base_it)
      , base_end_(base_end)
      , char_()
      , char_valid_(false)
    {}

    class End
    {};

    struct Iterator
    {
      char operator *()
      {
        bool& char_valid = liner_.char_valid_;

        if (char_valid)
        {
          char_valid = false;
        }
        else
        {
          liner_.char_ = *liner_.base_it_;
          ++liner_.base_it_;
        }

        return liner_.char_;
      }

      Iterator& operator ++() noexcept
      {
        return *this;
      }

      Iterator& operator ++(const int) noexcept
      {
        return *this;
      }

      friend bool operator ==(const Iterator& it, const End&) noexcept
      {
        return it.at_end();
      }

      friend bool operator !=(const Iterator& it, const End& end) noexcept
      {
        return !(it == end);
      }

    private:
      friend struct One_liner;

      explicit Iterator(One_liner& liner) noexcept
        : liner_(liner)
      {}

      bool at_end() const
      {
        bool& char_valid = liner_.char_valid_;
        char& ch = liner_.char_;

        if (!char_valid)
        {
          if (liner_.base_it_ == liner_.base_end_)
          {
            return true;
          }

          ch = *liner_.base_it_;
          ++liner_.base_it_;
          char_valid = true;
        }

        return ch == '\n' || ch == '\r' || ch == '\v';
      }

      One_liner& liner_;
    };

    Iterator begin() noexcept
    {
      return Iterator(*this);
    }

    End end() const noexcept
    {
      return {};
    }

  private:
    Base_iterator& base_it_;
    const Base_end& base_end_;
    char char_;
    bool char_valid_;
  };

  Hierarchy hierarchy_;
};

#endif // HIERARCHYSERVER_HH
