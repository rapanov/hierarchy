/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#include <cxx/util/hierarchy.hh>

namespace cxx::util
{

bool Hierarchy::add(const String& id, const String& name)
{
  return add(id, name, {});
}

bool Hierarchy::add(
  const String& id,
  const String& name,
  const Optional<String>& parent_id)
{
  using Offset = Vector<Index>::size_type;
  using Iterator = Vector<Index>::const_iterator;

  if (id.empty() || name.empty())
  {
    return false;
  }

  const Iterator begin = oredered_by_id_.cbegin();
  const Iterator end = oredered_by_id_.cend();
  const Iterator it = ::std::lower_bound(
    begin, end, id, Id_comparator(*this));

  if (it != end)
  {
    const Index idx = *it;

    if (idx >= nodes_.size() || nodes_[idx].id == id)
    {
      // Node with this id already exists.
      return false;
    }
  }

  const Offset node_offset = static_cast<Offset>(it - begin);
  Optional<Index> parent_idx;

  if (parent_id)
  {
    const String& parent_id_str = *parent_id;

    if (!parent_id_str.empty())
    {
      parent_idx = index_by_id(parent_id_str);

      if (!parent_idx)
      {
        // Non-existent parent.
        return false;
      }
    }
  }

  Offset child_offset;

  if (parent_idx)
  {
    const Node& parent = nodes_[*parent_idx];
    const Iterator child_begin = parent.children_indices.cbegin();
    const Iterator child_end = parent.children_indices.cend();
    const Iterator child_it = ::std::lower_bound(
      child_begin, child_end, name, Name_comparator(*this));

    if (child_it != child_end)
    {
      const Index child_idx = *child_it;

      if (child_idx >= nodes_.size() || nodes_[child_idx].name == name)
      {
        // Child with this name already exists.
        return false;
      }
    }

    child_offset = static_cast<Offset>(child_it - child_begin);
  }
  else if (root_idx_)
  {
    // Root already exists.
    return false;
  }

  const Index new_node_idx = alloc_node();
  Node& new_node = nodes_[new_node_idx];
  new_node.id = id;
  new_node.name = name;

  if (parent_idx)
  {
    Node& parent = nodes_[*parent_idx];
    new_node.parent_index = parent_idx;
    parent.children_indices.insert(
      parent.children_indices.cbegin() + child_offset, new_node_idx);
  }
  else
  {
    root_idx_.emplace(new_node_idx);
  }

  oredered_by_id_.insert(
    oredered_by_id_.cbegin() + node_offset, new_node_idx);
  return true;
}

bool Hierarchy::remove(const String& id)
{
  decltype(auto) end = oredered_by_id_.cend();
  decltype(auto) it = ::std::lower_bound(
    oredered_by_id_.cbegin(), end, id, Id_comparator(*this));

  if (it == end)
  {
    // Node with the specified id does not exist.
    return false;
  }

  const Index node_idx = *it;

  if (node_idx >= nodes_.size())
  {
    return false;
  }

  Node& node = nodes_[node_idx];

  if (node.id != id)
  {
    return false;
  }

  if (!node.children_indices.empty())
  {
    // Node to be removed has children.
    return false;
  }

  if (node.parent_index)
  {
    const Index parent_idx = *node.parent_index;

    if (parent_idx >= nodes_.size())
    {
      return false;
    }

    Node& parent = nodes_[parent_idx];
    decltype(auto) child_end = parent.children_indices.cend();
    decltype(auto) child_it = ::std::lower_bound(
      parent.children_indices.cbegin(), child_end,
      node.name, Name_comparator(*this));

    if (child_it == child_end || *child_it != node_idx)
    {
      return false;
    }

    parent.children_indices.erase(child_it);
  }
  else
  {
    root_idx_.reset();
  }

  vacant_indices_.emplace_back(node_idx);
  oredered_by_id_.erase(it);
  node.reset();
  return true;
}

bool Hierarchy::move(const String& id, const String& new_parent_id)
{
  const Optional<Index> node_idx = index_by_id(id);

  if (!node_idx)
  {
    return false;
  }

  const Optional<Index> new_parent_idx = index_by_id(new_parent_id);

  if (!new_parent_idx)
  {
    return false;
  }

  const Node& node = nodes_[*node_idx];
  const Optional<Index> old_parent_idx = node.parent_index;

  if (old_parent_idx && *old_parent_idx == *new_parent_idx)
  {
    // Already here.
    return true;
  }

  Index par_idx = *new_parent_idx;

  for (;;)
  {
    if (par_idx == *node_idx)
    {
      // Loop detected.
      return false;
    }

    const Node& par = nodes_[par_idx];

    if (!par.parent_index)
    {
      break;
    }

    par_idx = *par.parent_index;

    if (par_idx >= nodes_.size())
    {
      return false;
    }
  }

  if (!add_child(*new_parent_idx, *node_idx))
  {
    return false;
  }

  if (old_parent_idx)
  {
    if (!remove_child(*old_parent_idx, *node_idx))
    {
      return false;
    }
  }

  return true;
}

Hierarchy::Index Hierarchy::alloc_node()
{
  Index new_idx;

  if (vacant_indices_.empty())
  {
    new_idx = nodes_.size();
    nodes_.emplace_back();
  }
  else
  {
    new_idx = vacant_indices_.back();
    vacant_indices_.pop_back();
  }

  return new_idx;
}

Hierarchy::Optional<Hierarchy::Index>
Hierarchy::index_by_id(const String& id) noexcept
{
  decltype(auto) end = oredered_by_id_.cend();
  decltype(auto) it = ::std::lower_bound(
    oredered_by_id_.cbegin(), end,
    id, Id_comparator(*this));

  if (it == end)
  {
    return {};
  }

  const Index idx = *it;

  if (idx >= nodes_.size() || nodes_[idx].id != id)
  {
    return {};
  }

  return idx;
}

bool Hierarchy::add_child(const Index& parent_idx, const Index& child_idx)
{
  decltype(auto) node_count = nodes_.size();

  if (parent_idx >= node_count || child_idx >= node_count)
  {
    return false;
  }

  Node& parent_node = nodes_[parent_idx];
  Node& child_node = nodes_[child_idx];
  decltype(auto) end = parent_node.children_indices.cend();
  decltype(auto) it = ::std::lower_bound(
    parent_node.children_indices.cbegin(), end,
    child_node.name, Name_comparator(*this));

  if (it != end)
  {
    const Index idx = *it;

    if (idx >= node_count || nodes_[idx].name == child_node.name)
    {
      // Child with this name already exists.
      return false;
    }
  }

  parent_node.children_indices.insert(it, child_idx);
  child_node.parent_index = parent_idx;
  return true;
}

bool Hierarchy::remove_child(const Index& parent_idx, const Index& child_idx)
{
  decltype(auto) node_count = nodes_.size();

  if (parent_idx >= node_count || child_idx >= node_count)
  {
    return false;
  }

  Node& parent_node = nodes_[parent_idx];
  Node& child_node = nodes_[child_idx];
  decltype(auto) end = parent_node.children_indices.cend();
  decltype(auto) it = ::std::lower_bound(
    parent_node.children_indices.cbegin(), end,
    child_node.name, Name_comparator(*this));

  if (it == end)
  {
    return false;
  }

  const Index idx = *it;

  if (idx >= node_count || nodes_[idx].name != child_node.name)
  {
    return false;
  }

  parent_node.children_indices.erase(it);
  return true;
}

} // namespace cxx::util
