/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#ifndef CXX_UTIL_HIERARCHY_HH
#define CXX_UTIL_HIERARCHY_HH

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <limits>
#include <memory>
#include <optional>
#include <string>
#include <type_traits>
#include <vector>

namespace cxx::util
{

struct Hierarchy
{
  using String = ::std::string;
  using Depth = typename ::std::make_signed<::std::size_t>::type;

  template <class T>
  using Optional = ::std::optional<T>;

  bool add(const String& id, const String& name);
  bool add(
    const String& id,
    const String& name,
    const Optional<String>& parent_id);

  bool remove(const String& id);
  bool move(const String& id, const String& new_parent_id);

  template <class Consumer>
  void query(
    const Optional<String>& root_id,
    const Optional<Depth>& min_depth,
    const Optional<Depth>& max_depth,
    Consumer&& consumer)
  {
    Optional<Index> root_idx;

    if (root_id)
    {
      root_idx = index_by_id(*root_id);
    }
    else
    {
      root_idx = root_idx_;
    }

    if (!root_idx)
    {
      return;
    }

    const Depth min_depth_val = min_depth ? *min_depth : Limits<Depth>::min();
    const Depth max_depth_val = max_depth ? *max_depth : Limits<Depth>::max();
    do_query(
      *root_idx, min_depth_val, max_depth_val, Depth(0),
      static_cast<Consumer&&>(consumer));
  }

private:
  using Index = ::std::size_t;

  template <class T>
  using Limits = ::std::numeric_limits<T>;

  template <class T>
  using Vector = ::std::vector<T>;

  struct Node
  {
    void reset() noexcept
    {
      id.clear();
      name.clear();
      parent_index.reset();
      children_indices.clear();
    }

    String id;
    String name;
    Optional<Index> parent_index;
    Vector<Index> children_indices;
  };

  template <bool by_name>
  struct Comparator
  {
    Comparator(const Hierarchy& hierarchy) noexcept
      : hierarchy_(hierarchy)
    {}

    bool operator ()(const Index& node_idx, const String& x) const noexcept
    {
      if constexpr (by_name)
      {
        return hierarchy_.nodes_[node_idx].name < x;
      }
      else
      {
        return hierarchy_.nodes_[node_idx].id < x;
      }
    }

  private:
    const Hierarchy& hierarchy_;
  };

  using Id_comparator = Comparator<false>;
  using Name_comparator = Comparator<true>;

  template <class Consumer>
  void do_query(
    const Index& root_idx,
    const Depth& min_depth,
    const Depth& max_depth,
    const Depth& depth,
    Consumer&& consumer)
  {
    static const String empty;

    if (depth > max_depth || root_idx >= nodes_.size())
    {
      return;
    }

    const Node& root_node = nodes_[root_idx];

    if (depth >= min_depth)
    {
      const String* parent_id = ::std::addressof(empty);

      if (root_node.parent_index)
      {
        const Index parent_idx = *root_node.parent_index;

        if (parent_idx < nodes_.size())
        {
          parent_id = ::std::addressof(nodes_[parent_idx].id);
        }
      }

      static_cast<Consumer&&>(consumer)(
        root_node.id, root_node.name, *parent_id);
    }

    if (depth >= max_depth)
    {
      return;
    }

    for (const Index& child_idx : root_node.children_indices)
    {
      do_query(
        child_idx, min_depth, max_depth, depth + 1,
        static_cast<Consumer&&>(consumer));
    }
  }

  Index alloc_node();
  Optional<Index> index_by_id(const String& id) noexcept;
  bool add_child(const Index& parent_idx, const Index& child_idx);
  bool remove_child(const Index& parent_idx, const Index& child_idx);

  Vector<Node> nodes_;
  Vector<Index> oredered_by_id_;
  Vector<Index> vacant_indices_;
  Optional<Index> root_idx_;
};

} // namespace cxx::util

#endif // CXX_UTIL_HIERARCHY_HH
