/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#ifndef CXX_UTIL_JSON_EXCEPTION_HH
#define CXX_UTIL_JSON_EXCEPTION_HH

#include <exception>

namespace cxx::util::json
{

struct Exception : ::std::exception
{
  const char* what() const noexcept override
  {
    return "JSON error!!!";
  }
};

} // namespace cxx::util::json

#endif // CXX_UTIL_JSON_EXCEPTION_HH
