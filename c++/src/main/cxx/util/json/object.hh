/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#ifndef CXX_UTIL_JSON_OBJECT_XX
#define CXX_UTIL_JSON_OBJECT_XX

#include <cstddef>
#include <map>
#include <memory>
#include <string>
#include <tuple>
#include <utility>
#include <variant>
#include <vector>

namespace cxx::util::json
{

struct Object
{
  using Size = ::std::size_t;
  using String = ::std::string;

private:
  template <class T>
  using Vector = ::std::vector<T>;

  template <class K, class V>
  using Map = ::std::map<K, V>;

  template <class... T>
  using Variant = ::std::variant<T...>;

  template <class T>
  struct Value_impl
  {
    using Array = Vector<Value_impl>;

    Value_impl(String&& str) noexcept
      : val_(::std::in_place_type_t<String>(), static_cast<String&&>(str))
    {}

    Value_impl(T&& obj) noexcept
      : val_(::std::in_place_type_t<T>(), static_cast<T&&>(obj))
    {}

    Value_impl(Array&& arr) noexcept
      : val_(::std::in_place_type_t<Array>(), static_cast<Array&&>(arr))
    {}

    bool is_string() const noexcept
    {
      return ::std::holds_alternative<String>(val_);
    }

    bool is_object() const noexcept
    {
      return ::std::holds_alternative<T>(val_);
    }

    bool is_array() const noexcept
    {
      return ::std::holds_alternative<Array>(val_);
    }

    String& string() & noexcept
    {
      return ::std::get<String>(val_);
    }

    String&& string() && noexcept
    {
      return ::std::get<String>(::std::move(val_));
    }

    const String& string() const & noexcept
    {
      return ::std::get<String>(val_);
    }

    const String&& string() const && noexcept
    {
      return ::std::get<String>(::std::move(val_));
    }

    Object& object() & noexcept
    {
      return ::std::get<T>(val_);
    }

    Object&& object() && noexcept
    {
      return ::std::get<T>(::std::move(val_));
    }

    const Object& object() const & noexcept
    {
      return ::std::get<T>(val_);
    }

    const Object&& object() const && noexcept
    {
      return ::std::get<T>(::std::move(val_));
    }

    Array& array() & noexcept
    {
      return ::std::get<Array>(val_);
    }

    Array&& array() && noexcept
    {
      return ::std::get<Array>(::std::move(val_));
    }

    const Array& array() const & noexcept
    {
      return ::std::get<Array>(val_);
    }

    const Array&& array() const && noexcept
    {
      return ::std::get<Array>(::std::move(val_));
    }

    friend bool operator == (const Value_impl& x, const String& y) noexcept
    {
      return x.is_string() && ::std::get<String>(x.val_) == y;
    }

    friend bool operator == (const String& x, const Value_impl& y) noexcept
    {
      return y == x;
    }

    friend bool operator != (const String& x, const Value_impl& y) noexcept
    {
      return !(x == y);
    }

  private:
    Variant<String, T, Vector<Value_impl>> val_;
  };

  template <class M>
  static auto* get(const String& key, M& map) noexcept
  {
    decltype(auto) it = map.find(key);
    return it == map.end() ? nullptr : ::std::addressof(it->second);
  }

public:
  using Value = Value_impl<Object>;

  bool empty() const noexcept
  {
    return fields_.empty();
  }

  Size size() const noexcept
  {
    return fields_.size();
  }

  const Value* get(const String& key) const noexcept
  {
    return get(key, fields_);
  }

  Value* get(const String& key) noexcept
  {
    return get(key, fields_);
  }

  template <class V>
  void set(String&& key, V&& value)
  {
    fields_.emplace(
      ::std::piecewise_construct,
      ::std::forward_as_tuple(static_cast<String&&>(key)),
      ::std::forward_as_tuple(static_cast<V&&>(value)));
  }

  decltype(auto) begin() const noexcept
  {
    return fields_.begin();
  }

  decltype(auto) end() const noexcept
  {
    return fields_.end();
  }

private:
  Map<String, Value> fields_;
};

} // namespace cxx::util::json

#endif // CXX_UTIL_JSON_OBJECT_XX
