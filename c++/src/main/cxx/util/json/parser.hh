/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#ifndef CXX_UTIL_JSON_PARSER_XX
#define CXX_UTIL_JSON_PARSER_XX

#include <cassert>
#include <cctype>
#include <vector>
#include <cxx/util/json/exception.hh>
#include <cxx/util/json/object.hh>

namespace cxx::util::json
{

struct Parser
{
  using String = ::std::string;
  using Object = ::cxx::util::json::Object;

  template <class Input, class Listener>
  void parse(Input&& input, Listener&& listener)
  {
    decltype(auto) it = static_cast<Input&&>(input).begin();
    decltype(auto) it_end = static_cast<Input&&>(input).end();

    if (it == it_end)
    {
      return;
    }

    reset();
    parse_object(it, it_end, static_cast<Listener&&>(listener));
  }

  template <class Input>
  Object parse(Input&& input)
  {
    using Array = Object::Value::Array;

    enum class State
    {
      in_object = 0,
      in_array,
      got_key
    };

    struct Builder
    {
      void on_begin_object()
      {
        obj_stack.emplace_back();
        state_stack.emplace_back(State::in_object);
      }

      void on_end_object()
      {
        if (state_stack.empty() || state_stack.back() != State::in_object)
        {
          throw Exception();
        }

        state_stack.pop_back();

        if (state_stack.empty())
        {
          return;
        }

        if (obj_stack.empty())
        {
          throw Exception();
        }

        const State& state = state_stack.back();

        switch (state)
        {
        case State::in_array:
          if (array_stack_.empty())
          {
            throw Exception();
          }

          array_stack_.back().emplace_back(
            static_cast<Object&&>(obj_stack.back()));
          obj_stack.pop_back();
          break;

        case State::got_key:
          {
            decltype(auto) obj_count = obj_stack.size();

            if (key_stack_.empty() || obj_count < 2U)
            {
              throw Exception();
            }

            Object& obj = obj_stack[obj_count - 2U];
            String& key = key_stack_.back();
            Object& val = obj_stack.back();
            obj.set(static_cast<String&&>(key), static_cast<Object&&>(val));
            key_stack_.pop_back();
            obj_stack.pop_back();
            state_stack.pop_back();
          }

          break;

        default:
          throw Exception();
        }
      }

      void on_begin_array()
      {
        if (state_stack.empty())
        {
          throw Exception();
        }

        const State& state = state_stack.back();

        if (state != State::in_array && state != State::got_key)
        {
          throw Exception();
        }

        state_stack.emplace_back(State::in_array);
        array_stack_.emplace_back();
      }

      void on_end_array()
      {
        if (state_stack.empty() || state_stack.back() != State::in_array)
        {
          throw Exception();
        }

        state_stack.pop_back();

        if (state_stack.empty() || array_stack_.empty())
        {
          throw Exception();
        }

        const State& state = state_stack.back();

        switch (state)
        {
        case State::in_array:
          {
            decltype(auto) array_count = array_stack_.size();

            if (array_count < 2U)
            {
              throw Exception();
            }

            array_stack_[array_count - 2U].
              emplace_back(static_cast<Array&&>(array_stack_.back()));
            array_stack_.pop_back();
          }

          break;

        case State::got_key:
          if (!obj_stack.empty() && !key_stack_.empty())
          {
            Object& obj = obj_stack.back();
            String& key = key_stack_.back();
            Array& val = array_stack_.back();
            obj.set(static_cast<String&&>(key), static_cast<Array&&>(val));
            key_stack_.pop_back();
            array_stack_.pop_back();
            state_stack.pop_back();
          }
          else
          {
            throw Exception();
          }

          break;

        default:
          throw Exception();
        }
      }

      void on_literal(const String& literal)
      {
        if (state_stack.empty())
        {
          return;
        }

        const State& state = state_stack.back();

        switch (state)
        {
        case State::in_object:
          key_stack_.emplace_back(literal);
          state_stack.emplace_back(State::got_key);
          break;

        case State::in_array:
          if (array_stack_.empty())
          {
            throw Exception();
          }

          array_stack_.back().emplace_back(String(literal));
          break;

        case State::got_key:
          if (!obj_stack.empty() && !key_stack_.empty())
          {
            Object& obj = obj_stack.back();
            String& key = key_stack_.back();
            obj.set(static_cast<String&&>(key), String(literal));
            key_stack_.pop_back();
            state_stack.pop_back();
          }
          else
          {
            throw Exception();
          }

          break;

        default:
          throw Exception();
        }
      }

      Vector<Object> obj_stack;
      Vector<State> state_stack;

    private:
      Vector<String> key_stack_;
      Vector<Array> array_stack_;
    };

    Builder builder;
    parse(static_cast<Input&&>(input), builder);

    if (builder.state_stack.empty())
    {
      if (builder.obj_stack.empty())
      {
        return {};
      }

      if (builder.obj_stack.size() == 1U)
      {
        return static_cast<Object&&>(builder.obj_stack.front());
      }
    }

    throw Exception();
  }

private:
  using Exception = ::cxx::util::json::Exception;

  template <class T>
  using Vector = ::std::vector<T>;

  enum class Token_kind
  {
    none = 0,
    colon,
    comma,
    left_brace,
    right_brace,
    left_bracket,
    right_bracket,
    literal
  };

  void reset() noexcept
  {
    token_str_.clear();
    token_kind_ = Token_kind::none;
    char_valid_ = false;
  }

  template <class Iter, class Iter_end, class Listener>
  void parse_object(Iter& it, const Iter_end& it_end, Listener&& listener)
  {
    read_token(it, it_end);

    if (token_kind_ != Token_kind::left_brace)
    {
      throw Exception();
    }

    consume_token();
    static_cast<Listener&&>(listener).on_begin_object();

    for (;;)
    {
      read_token(it, it_end);

      switch (token_kind_)
      {
      case Token_kind::right_brace:
        consume_token();
        static_cast<Listener&&>(listener).on_end_object();
        return;

      case Token_kind::literal:
        consume_token();
        static_cast<Listener&&>(listener).on_literal(
          static_cast<const String&>(token_str_));
        break;

      default:
        throw Exception();
      }

      read_token(it, it_end);

      if (token_kind_ != Token_kind::colon)
      {
        throw Exception();
      }

      consume_token();
      read_token(it, it_end);

      switch (token_kind_)
      {
      case Token_kind::left_brace:
        parse_object(it, it_end, static_cast<Listener&&>(listener));
        break;

      case Token_kind::left_bracket:
        parse_array(it, it_end, static_cast<Listener&&>(listener));
        break;

      case Token_kind::literal:
        consume_token();
        static_cast<Listener&&>(listener).on_literal(
          static_cast<const String&>(token_str_));
        break;

      default:
        throw Exception();
      }

      read_token(it, it_end);

      if (token_kind_ == Token_kind::comma)
      {
        // Ignore commas among fields.
        consume_token();
      }
    }
  }

  template <class Iter, class Iter_end, class Listener>
  void parse_array(Iter& it, const Iter_end& it_end, Listener&& listener)
  {
    read_token(it, it_end);

    if (token_kind_ != Token_kind::left_bracket)
    {
      throw Exception();
    }

    static_cast<Listener&&>(listener).on_begin_array();
    consume_token();

    for (;;)
    {
      read_token(it, it_end);

      switch (token_kind_)
      {
      case Token_kind::left_brace:
        parse_object(it, it_end, static_cast<Listener&&>(listener));
        break;

      case Token_kind::left_bracket:
        parse_array(it, it_end, static_cast<Listener&&>(listener));
        break;

      case Token_kind::right_bracket:
        consume_token();
        static_cast<Listener&&>(listener).on_end_array();
        return;

      case Token_kind::literal:
        consume_token();
        static_cast<Listener&&>(listener).on_literal(
          static_cast<const String&>(token_str_));
        break;

      default:
        throw Exception();
      }

      read_token(it, it_end);

      if (token_kind_ == Token_kind::comma)
      {
        consume_token();
      }
      else if (token_kind_ != Token_kind::right_bracket)
      {
        throw Exception();
      }
    }
  }

  template <class Iter, class Iter_end>
  void read_token(Iter& it, const Iter_end& it_end)
  {
    enum class State
    {
      initial = 0,
      literal,
      escape
    };

    if (token_kind_ != Token_kind::none)
    {
      return;
    }

    token_str_.clear();
    State state = State::initial;
    bool done = false;
    bool single_quated = false;
    bool double_quated = false;

    while (!done && read_char(it, it_end))
    {
      switch (state)
      {
      case State::initial:
        {
          const Token_kind token_kind = char_to_token(char_);

          if (token_kind != Token_kind::none)
          {
            token_kind_ = token_kind;
            done = true;
            consume_char();
          }
          else if (is_space(char_))
          {
            consume_char();
          }
          else
          {
            state = State::literal;

            switch (char_)
            {
            case '\'':
              single_quated = true;
              break;
            case '\"':
              double_quated = true;
              break;
            default:
              token_str_.push_back(char_);
            }

            consume_char();
          }
        }

        break;

      case State::literal:
        if (single_quated)
        {
          switch (char_)
          {
          case '\'':
            done = true;
            consume_char();
            break;
          case '\\':
            state = State::escape;
            consume_char();
            break;
          }
        }
        else if (double_quated)
        {
          switch (char_)
          {
          case '\"':
            done = true;
            consume_char();
            break;
          case '\\':
            state = State::escape;
            consume_char();
            break;
          }
        }
        else if (is_space(char_) || char_to_token(char_) != Token_kind::none)
        {
          done = true;
        }

        if (done)
        {
          state = State::initial;
          token_kind_ = Token_kind::literal;
        }
        else if (state != State::escape)
        {
          token_str_.push_back(char_);
          consume_char();
        }

        break;

      case State::escape:
        state = State::literal;
        token_str_.push_back(char_);
        consume_char();
        break;

      default:
        assert(false);
      }
    }
  }

  void consume_token() noexcept
  {
    token_kind_ = Token_kind::none;
  }

  void consume_char() noexcept
  {
    char_valid_ = false;
  }

  template <class Iter, class Iter_end>
  bool read_char(Iter& it, const Iter_end& it_end)
  {
    if (char_valid_)
    {
      return true;
    }

    if (it == it_end)
    {
      return false;
    }

    char_ = *it;
    ++it;
    char_valid_ = true;
    return true;
  }

  static bool is_ascii(const char ch) noexcept
  {
    return static_cast<unsigned char>(ch) < 0x80U;
  }

  static bool is_space(const char ch) noexcept
  {
    return is_ascii(ch) && ::std::isspace(ch);
  }

  static Token_kind char_to_token(const char ch) noexcept
  {
    switch (ch)
    {
    case ':':
      return Token_kind::colon;
    case ',':
      return Token_kind::comma;
    case '{':
      return Token_kind::left_brace;
    case '}':
      return Token_kind::right_brace;
    case '[':
      return Token_kind::left_bracket;
    case ']':
      return Token_kind::right_bracket;
    }

    return Token_kind::none;
  }

  String token_str_;
  Token_kind token_kind_ = Token_kind::none;
  char char_ = {};
  bool char_valid_ = false;
};

} // namespace cxx::util::json

#endif // CXX_UTIL_JSON_PARSER_XX
