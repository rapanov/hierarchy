/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#include <hierarchy_server.hh>

bool Hierarchy_server::add_node(const Object::Value& request)
{
  if (!request.is_object())
  {
    return false;
  }

  const Object& obj = request.object();
  decltype(auto) id = get_string(obj, "id");

  if (!id)
  {
    return false;
  }

  decltype(auto) name = get_string(obj, "name");

  if (!name)
  {
    return false;
  }

  decltype(auto) parent_id = get_string(obj, "parent_id");
  return hierarchy_.add(*id, *name, parent_id);
}

bool Hierarchy_server::delete_node(const Object::Value& request)
{
  if (!request.is_object())
  {
    return false;
  }

  decltype(auto) id = get_string(request.object(), "id");

  if (!id)
  {
    return false;
  }

  return hierarchy_.remove(*id);
}

bool Hierarchy_server::move_node(const Object::Value& request)
{
  if (!request.is_object())
  {
    return false;
  }

  const Object& obj = request.object();
  decltype(auto) id = get_string(obj, "id");

  if (!id)
  {
    return false;
  }

  decltype(auto) new_parent_id = get_string(obj, "new_parent_id");

  if (!new_parent_id)
  {
    return false;
  }

  return hierarchy_.move(*id, *new_parent_id);
}

Hierarchy_server::Optional<Hierarchy_server::String>
Hierarchy_server::get_string(const Object& obj, const String& key)
{
  decltype(auto) val = obj.get(key);

  if (!val || !val->is_string())
  {
    return {};
  }

  return val->string();
}
