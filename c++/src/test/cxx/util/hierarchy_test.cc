/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */


#include <catch.hpp>
#include <cxx/util/hierarchy.hh>

namespace cxx::util
{

TEST_CASE("cxx::util::Hierarchy")
{
  ::cxx::util::Hierarchy h;

  CHECK(h.add("1", "Root"));
  CHECK(h.add("4", "Child42", "1"));
  CHECK(h.add("5", "Node~2", "1"));
  CHECK(h.add("3", "Node~0", "1"));
  CHECK(h.add("12", "Node~2", "5"));
  CHECK(h.add("13", "Node~13", "5"));
  CHECK(!h.add("100", "Root"));
  CHECK(!h.add("22", "Child42", "1"));

  CHECK(h.remove("12"));
  CHECK(!h.remove("5"));
  CHECK(h.remove("4"));
  CHECK(h.remove("3"));
  CHECK(!h.remove("1"));
  CHECK(h.remove("13"));
  CHECK(h.remove("5"));
  CHECK(h.remove("1"));

  CHECK(h.add("x-000", "Root"));
  CHECK(h.add("x-001", "L-10", "x-000"));
  CHECK(h.add("x-002", "L-11", "x-000"));
  CHECK(h.add("x-004", "L-20", "x-001"));
  CHECK(h.move("x-004", "x-002"));
  CHECK(h.move("x-004", "x-000"));
}

} // namespace cxx::util
