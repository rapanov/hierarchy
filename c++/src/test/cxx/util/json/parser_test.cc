/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#include <string>
#include <catch.hpp>
#include <cxx/util/json/object.hh>
#include <cxx/util/json/parser.hh>

namespace cxx::util::json
{

struct Parser_test
{
  using String = ::std::string;
  using Object = ::cxx::util::json::Object;

  static Object parse(const String& str)
  {
    ::cxx::util::json::Parser parser;
    return parser.parse(str);
  }
};

TEST_CASE("cxx::util::json::Parser")
{
  using Object = Parser_test::Object;
  using String = Parser_test::String;
  using Value = Object::Value;

  SECTION("")
  {
    String text;

    SECTION("")
    {
      text = "{key : value}";
    }

    SECTION("")
    {
      text = "{key:value}";
    }

    SECTION("")
    {
      text = "{\"key\" :\'value\'}";
    }

    const Object obj = Parser_test::parse(text);
    const Value* const val = obj.get("key");

    CHECK(obj.size() == 1U);
    REQUIRE(val);
    CHECK(*val == "value");
    CHECK("value" == *val);
  }

  SECTION("")
  {
    const String text = "{\"k\\\'e\\\"y\" :    \'v\\\'a\\\"l\'}";
    const Object obj = Parser_test::parse(text);
    const Value* const val = obj.get("k\'e\"y");

    CHECK(obj.size() == 1U);
    REQUIRE(val);
    CHECK(*val == "v\'a\"l");
  }

  SECTION("")
  {
    const String text = "{\"a: b , c\" : \"val\"}";
    const Object obj = Parser_test::parse(text);
    const Value* const val = obj.get("a: b , c");

    CHECK(obj.size() == 1U);
    REQUIRE(val);
    CHECK(*val == "val");
  }

  SECTION("")
  {
    const String text = "{key_0 : val_0, key_1 : val_1}";
    const Object obj = Parser_test::parse(text);
    const Value* const val_0 = obj.get("key_0");
    const Value* const val_1 = obj.get("key_1");

    CHECK(obj.size() == 2U);
    REQUIRE(val_0);
    REQUIRE(val_1);
    CHECK(*val_0 == "val_0");
    CHECK(*val_1 == "val_1");
  }

  SECTION("")
  {
    const String text = "{key_0 : {a:b, x:y}, key_1 : val_1, key_2 : val_2}";
    const Object obj = Parser_test::parse(text);
    const Value* const val_0 = obj.get("key_0");
    const Value* const val_1 = obj.get("key_1");
    const Value* const val_2 = obj.get("key_2");

    CHECK(obj.size() == 3U);
    REQUIRE(val_0);
    REQUIRE(val_1);
    REQUIRE(val_2);
    CHECK(*val_1 == "val_1");
    CHECK(*val_2 == "val_2");

    REQUIRE(val_0->is_object());

    const Object& subobj = val_0->object();
    const Value* const a = subobj.get("a");
    const Value* const x = subobj.get("x");

    CHECK(subobj.size() == 2U);
    REQUIRE(a);
    REQUIRE(x);
    CHECK(*a == "b");
    CHECK(*x == "y");
  }

  SECTION("")
  {
    const String text =
      "{key_0 : val_0, key_1 : [a, {b : y}, [4, i], z], key_2 : val_2}";

    const Object obj = Parser_test::parse(text);
    const Value* const val_0 = obj.get("key_0");
    const Value* const val_1 = obj.get("key_1");
    const Value* const val_2 = obj.get("key_2");

    CHECK(obj.size() == 3U);
    REQUIRE(val_0);
    REQUIRE(val_1);
    REQUIRE(val_2);
    CHECK(*val_0 == "val_0");
    CHECK(*val_2 == "val_2");
    REQUIRE(val_1->is_array());

    const auto& arr = val_1->array();
    REQUIRE(arr.size() == 4U);
    CHECK(arr[0U] == "a");
    CHECK(arr[3U] == "z");

    const Value& elem_1 = arr[1U];
    REQUIRE(elem_1.is_object());

    const Object& subobj = elem_1.object();
    CHECK(subobj.size() == 1U);

    const Value* const b = subobj.get("b");
    REQUIRE(b);
    CHECK(*b == "y");

    const Value& elem_2 = arr[2U];
    REQUIRE(elem_2.is_array());

    const auto& subarr = elem_2.array();
    REQUIRE(subarr.size() == 2U);
    CHECK(subarr[0U] == "4");
    CHECK(subarr[1U] == "i");
  }
}

} // namespace cxx::util::json
